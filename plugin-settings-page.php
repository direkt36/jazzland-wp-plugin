<?php

if(isset($_POST['d36_submit'])) {
    $d36_domain = $_POST['d36_domain'];
    update_option('d36_domain', $d36_domain);

    echo '<div class="updated"><p><strong>Beállítások mentve</strong></p></div>';
} else {
    $d36_domain = get_option('d36_domain');
}

?>

<div class="wrap">
    <?php echo '<h2>Direkt36 Contributions</h2>'; ?>

    <form name="d36-contributions-settings" method="post" action="<?php echo str_replace('%7E', '~', $_SERVER['REQUEST_URI']); ?>">
        <p>Domain: <input type="text" name="d36_domain" value="<?php echo $d36_domain; ?>" size="30"></p>
        <p class="submit"><input type="submit" name="d36_submit" value="Mentés" /></p>
    </form>
</div>
