<?php

/*
Plugin Name: Direkt36 Contributions
Plugin URI: http://www.biroda.net
Description: Plugin for writing comments on Direkt36
Author: Biroda Kft.
Version: 1.0
Author URI: http://www.biroda.net
*/

class D36Contributions
{
    public function __construct()
    {
        add_action('add_meta_boxes', array($this, 'addD36MetaBox'));
        add_action('save_post', array($this, 'updateTopicId'));
        add_filter('the_content', array($this, 'renderContributions'));
        add_filter('comments_open', array($this, 'showDefaultCommentsForm'));
        add_filter('comments_array', array($this, 'hideDefaultComments'));

        add_action('admin_menu', array($this, 'addPluginSettingsPage'));
    }

    public function addD36MetaBox()
    {
        add_meta_box('d36Contributions', 'Direkt36 Contributions', array($this, 'renderD36MetaBox'), 'post');
    }

    public function renderD36MetaBox($post)
    {
        ?>
        <input type="checkbox" name="d36ContributionsAllowed" id="d36ContributionsAllowed" class="postbox" <?php if (get_post_meta($post->ID, 'd36ContributionsAllowed', true)) echo 'checked'; ?> />
        <?php
    }

    public function updateTopicId($postId)
    {
        update_post_meta($postId, 'd36ContributionsAllowed', $_POST['d36ContributionsAllowed']);
    }

    public function renderContributions($content)
    {
        if ($this->postAllowsD36Contributions()) {
            $d36Domain = get_option('d36_domain');

            $permalink = get_permalink();
            $permalink = substr($permalink, stripos($permalink, '://') + 3); // Remove http:// from the start of the permalink
            $permalink = substr($permalink, 0, -1); // Remove trailing slash from permalink
            $permalink = str_ireplace(['/', '.'], '-', $permalink); // Replace slashes and dots with hyphens

            return $content .
                '<link rel="stylesheet" media="all" href="https://' . $d36Domain . '/assets/application.css">
                <div class="comment_section_wrapper" ng-app="zetland">
                    <div class="comment_section" ng-controller="commentController as main" ng-init="setTopic(\'' . $permalink . '\')">
                        <zetlandapp></zetlandapp>
                    </div>
                </div>
                <script type="text/javascript" src="https://' . $d36Domain . '/assets/application.js"></script>
                <script src="https://use.fontawesome.com/3382267086.js"></script>';
        }

        return $content;
    }

    private function postAllowsD36Contributions()
    {
        if (!$postId = get_the_ID()) {
            return false;
        }

        return get_post_meta($postId, 'd36ContributionsAllowed', true) ? true : false;
    }

    public function showDefaultCommentsForm()
    {
        if (
            $this->postAllowsD36Contributions() ||
            (post_type_supports(get_post_type(), 'comments') && get_post(get_the_ID())->comment_status != 'open')
        ) {
            return false;
        }

        return true;
    }

    public function hideDefaultComments()
    {
        if ($this->postAllowsD36Contributions()) {
            remove_post_type_support('post', 'comments');
            return [];
        }

        return get_comments(['post_id' => get_the_ID()]);
    }

    public function addPluginSettingsPage()
    {
        add_options_page('Direkt36 Contributions', 'Direkt36 Contributions', 1, 'Direkt36_Contributions', array($this, 'renderPluginSettingsPage'));
    }

    public function renderPluginSettingsPage()
    {
        include 'plugin-settings-page.php';
    }
}

$d36Contributions = new D36Contributions();

?>
