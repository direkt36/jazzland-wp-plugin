Jazzland WordPress Plugin

This plugin will add a checkbox to your wordpress Post edit page.
If that checkbox is checked, the default Wordpress comment will be replaced by the jazzland commenting system.

Set API url in Wordpress -> Settings -> Jazzland

Thats all!
